import sqlite3
import time

conn = sqlite3.connect('probemon.db')
c = conn.cursor()

c.execute('select date,mac.address from probemon inner join mac on mac.id=probemon.mac')

dop = {}
days = set()
for date,mac in c.fetchall():
    if mac not in dop:
        dop[mac] = set()
    day = time.strftime('%Y-%m-%d', time.localtime(date))
    days.add(day)
    dop[mac].add(day)

conn.close()

res = sorted(dop.items(), key=lambda x:len(x[1]))

digit = len('%d' % len(days))
template = '%%s %%%dd' % digit
for m, s in res:
    print template % (m, len(s))
print len(days)
